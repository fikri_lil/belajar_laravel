<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home(){
        return view('halaman.index');
    }
    
    public function kirim(Request $request){
        $nama = $request['name'];

        return view('welcome', ['nama'=> $nama]);
    }
}

